
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {FormBuilder,FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  data: any;
  submitted = false;

  constructor(private fb:FormBuilder, private router:Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      password: ['', Validators.required],
    });

  }

  get f() { return this.loginForm.controls; }

  loingPos(data) {
    this.data = data;

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    } else {
      console.log(this.data);
      this.router.navigateByUrl('/app/dashboard');
    }

  }

}
