import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { ListComponent } from './components/list/list.component';
import { AddComponent } from './components/add/add.component';
import {  MatButtonModule,
          MatCheckboxModule, 
          MatTableModule,
          MatToolbarModule, 
          MatChipsModule, 
          MatOptionModule, 
          MatGridListModule, 
          MatProgressBarModule, 
          MatSliderModule, 
          MatSlideToggleModule, 
          MatMenuModule, 
          MatDialogModule, 
          MatSnackBarModule, 
          MatSelectModule, 
          MatInputModule, 
          MatSidenavModule, 
          MatCardModule, 
          MatIconModule, 
          MatRadioModule, 
          MatProgressSpinnerModule, 
          MatTabsModule, 
          MatListModule,
          MatDatepickerModule } from '@angular/material';
          import {MatNativeDateModule} from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [CustomersComponent, ListComponent, AddComponent],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    MatButtonModule, 
    MatCheckboxModule, 
    MatToolbarModule, 
    MatTableModule,
    MatChipsModule, 
    MatOptionModule, 
    MatGridListModule, 
    MatProgressBarModule, 
    MatSliderModule, 
    MatSlideToggleModule, 
    MatMenuModule, 
    MatDialogModule, 
    MatSnackBarModule, 
    MatSelectModule, 
    MatInputModule, 
    MatSidenavModule, 
    MatCardModule, 
    MatIconModule, 
    MatRadioModule, 
    MatProgressSpinnerModule, 
    MatTabsModule, 
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule
  ],
  entryComponents:[AddComponent]
})
export class CustomersModule { }
