import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomersComponent } from './customers.component';
import { ListComponent } from './components/list/list.component';


const routes: Routes = [
  {
    path:'',
    component:CustomersComponent,
    children: [
      {
        path:'',
        component:ListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomersRoutingModule { }
