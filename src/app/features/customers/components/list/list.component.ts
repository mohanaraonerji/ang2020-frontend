import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddComponent } from '../add/add.component';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  customerData: any;
  displayedColumns: string[] = ['name', 'dob', 'gender', 'Qualification' ,'actions'];
  dataSource:any = new MatTableDataSource();
  users: any;
  customer_info: any;
  addressIndex: number;
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
    this.customerData = JSON.parse(localStorage.getItem('cusData'));
    this.customer_info = this.customerData.Customer;
    console.log("Customer:" +this.customerData)
    this.getCustomers();
  }

  addCustomer(): void {
    const dialogRef = this.dialog.open(AddComponent, {
      width: '640px', disableClose: true
    });
  }

  editCustomer(cinfo): void {
    this.addressIndex = this.customer_info.indexOf(cinfo);
    const dialogRef = this.dialog.open(AddComponent, {
      width: '640px', disableClose: true,
      data: cinfo
    });
  }

  // addAddress(): void {
  //   let data = {
  //     'address': {},
  //     'userData': this.user_info
  //   }
  //   const dialogRef = this.dialog.open(AddaddressmodelComponent, {
  //     width: '650px',
  //     data: data
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  // editAddress(address): void {
  //   let data = {
  //     'address': address,
  //     'userData': this.user_info
  //   }
  //   this.addressIndex = this.user_info.addresses.indexOf(address);
  //   const dialogRef = this.dialog.open(AddaddressmodelComponent, {
  //     width: '650px',
  //     data: data
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  getCustomers() {
    this.customerData = JSON.parse(localStorage.getItem('cusData'));
    this.users = this.customerData.Customer;
    this.dataSource = new MatTableDataSource(this.users);
  }

  // customerUpdate(element) {
  //   const dialogRef = this.dialog.open(AddComponent, {
  //     width: '640px', disableClose: true,
  //     data: {
  //       data: element
  //     }
  //   });
  // }

  customerDelete(element) {
      
  }
}
