import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

interface Qualification {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  public addCusForm: FormGroup;
  public breakpoint: number; 
  obj: any;
  submitted = false;
  selectDisabled = true;
  gender: string[] = ['Male', 'Female'];
  languages: string[] = ['Telugu', 'Hindi', 'English'];
  edit: boolean;
  user_info:any;
  constructor(public dialogRef: MatDialogRef<AddComponent>, private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data:any,private api:ApiService, public dialog: MatDialog) {
      this.user_info = data;
      if(this.user_info && this.user_info.hasOwnProperty('firstname')) {
        this.edit = true;
      } else {
        this.edit = false;
      }
     }

  ngOnInit() {
    this.user_info = this.data;
    this.addCusForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      dob: ['', Validators.required],
      gender: ['', Validators.required],
      comments: ['', Validators.required],
      qualification: ['', Validators.required],
      line1 : ['', Validators.required], 
      line2: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      pincode: ['', Validators.required],
      country: ['India', Validators.required],
    });
    this.breakpoint = window.innerWidth <= 600 ? 1 : 2; 

    if(this.edit) {
      this.addCusForm.setValue({
        'firstname': this.user_info.firstname,
        'lastname': this.user_info.lastname,
        'email': this.user_info.email,
        'mobile': this.user_info.mobile,
        'dob': this.user_info.dob,
        'gender': this.user_info.gender,
        'comments': this.user_info.comments,
        'qualification': this.user_info.qualification,
        'line1': this.user_info.Address[0].line1,
        'line2': this.user_info.Address[0].line2,
        'city': this.user_info.Address[0].city,
        'state': this.user_info.Address[0].state,
        'pincode': this.user_info.Address[0].pincode,
        'country': this.user_info.Address[0].country
      })
    }
  }

  qualification: Qualification[] = [
    {value: 'ssc', viewValue: 'SSC'},
    {value: 'inter', viewValue: 'Intermediate'},
    {value: 'degree', viewValue: 'Degree'},
    {value: 'btech', viewValue: 'B.Tech'},
    {value: 'pg', viewValue: 'Post Graduate'},
    {value: 'other', viewValue: 'Other'}
  ];

  addCustomer(data) {
    this.submitted = true;
    // stop here if form is invalid
    if (this.addCusForm.invalid) {
      return;
    }else {
    this.data = data;
    this.obj = {};
    this.obj.Customer = [{
      "firstname": data.firstname,
      "lastname": data.lastname,
      "email": data.email,
      "mobile": data.mobile,
      "dob": data.dob,
      "gender": data.gender,
      "comments": data.comments,
      "qualification": data.qualification,
      "Address": [
        {
          "line1": data.line1,
          "line2": data.line2,
          "city": data.city,
          "state": data.state,
          "pincode": data.pincode,
          "country": data.country,
        }
      ],
    }]
    localStorage.setItem('cusData', JSON.stringify(this.obj));
    this.dialogRef.close();
    location.reload();
    }
  }

  // Numbers Only 
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  
  // Pincode

  PinCodeCheck(e) {
    if(e.target.value && e.target.value.length === 6) {
      this.api.checkPinCode(e.target.value).subscribe(x => {
        console.log(x)
        if(x && x['records'] && x['records'].length) {
          let st = x['records'][0].statename.toLowerCase();
          st = this.titleCase(st)
          console.log(st)
          // if(!this.ShippingaddressFormGroup.get('ShippingCity').value)
            this.addCusForm.get('city').setValue(x['records'][0].districtname);
          this.addCusForm.get('state').setValue(st);
        }
      })
    }
  }
  titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        // You do not need to check if i is larger than splitStr length, as your for does that for you
        // Assign it back to the array
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
    }
    // Directly return the joined string
    return splitStr.join(' '); 
  }
}
