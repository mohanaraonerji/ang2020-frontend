import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MatButtonModule, 
  MatCheckboxModule, 
  MatToolbarModule, 
  MatChipsModule, 
  MatOptionModule, 
  MatGridListModule, 
  MatProgressBarModule, 
  MatSliderModule, 
  MatSlideToggleModule, 
  MatMenuModule, 
  MatDialogModule, 
  MatSnackBarModule, 
  MatSelectModule, 
  MatInputModule, 
  MatSidenavModule, 
  MatCardModule, 
  MatIconModule, 
  MatRadioModule, 
  MatProgressSpinnerModule, 
  MatTabsModule, 
  MatListModule } from '@angular/material';
import { AddmodalComponent } from './addmodal/addmodal.component';

@NgModule({
  declarations: [DashboardComponent, AddmodalComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    DashboardRoutingModule,
    MatCheckboxModule, 
    MatToolbarModule, 
    MatChipsModule, 
    MatOptionModule, 
    MatGridListModule, 
    MatProgressBarModule, 
    MatSliderModule, 
    MatSlideToggleModule, 
    MatMenuModule, 
    MatDialogModule, 
    MatSnackBarModule, 
    MatSelectModule, 
    MatInputModule, 
    MatSidenavModule, 
    MatCardModule, 
    MatIconModule, 
    MatRadioModule, 
    MatProgressSpinnerModule, 
    MatTabsModule, 
    MatListModule
  ],
  entryComponents:[AddmodalComponent]
})
export class DashboardModule { }
