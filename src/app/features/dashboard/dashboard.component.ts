import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AddmodalComponent } from './addmodal/addmodal.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AddmodalComponent, {
      width: '250px',
    });
  }
}
