import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { MatButtonModule, 
         MatCheckboxModule, 
         MatToolbarModule, 
         MatChipsModule, 
         MatOptionModule, 
         MatGridListModule, 
         MatProgressBarModule, 
         MatSliderModule, 
         MatSlideToggleModule, 
         MatMenuModule, 
         MatDialogModule, 
         MatSnackBarModule, 
         MatSelectModule, 
         MatInputModule, 
         MatSidenavModule, 
         MatCardModule, 
         MatIconModule, 
         MatRadioModule, 
         MatProgressSpinnerModule, 
         MatTabsModule, 
         MatListModule } from '@angular/material';
@NgModule({
  declarations: [FeaturesComponent],
  imports: [
    CommonModule,
    FeaturesRoutingModule,
    MatDialogModule,
    MatButtonModule, 
    MatCheckboxModule, 
    MatToolbarModule, 
    MatChipsModule, 
    MatOptionModule, 
    MatGridListModule, 
    MatProgressBarModule, 
    MatSliderModule, 
    MatSlideToggleModule, 
    MatMenuModule, 
    MatDialogModule, 
    MatSnackBarModule, 
    MatSelectModule, 
    MatInputModule, 
    MatSidenavModule, 
    MatCardModule, 
    MatIconModule, 
    MatRadioModule, 
    MatProgressSpinnerModule, 
    MatTabsModule, 
    MatListModule
  ]
})
export class FeaturesModule { }
